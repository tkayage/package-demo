
module.exports = {

    run: function(Todo, callback){
        Todo.find().exec(function(err, todos){
            if(err)
                callback({err: err});
            
            if(todos){
                callback({todos:todos});
            }
        });
    }
}

//New features added
